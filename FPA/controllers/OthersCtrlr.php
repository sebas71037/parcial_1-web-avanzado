<?php

class OthersCtrlr extends Controller{

    public function crear(){
        $types = Type::getAll();
        $sexs = Sex::getAll();
        $attacks = Attack::getAll();
        $phases = Phase::getAll();
        $citys = City::getAll();
        $vars = array("types"=>$types, "sexs" => $sexs, "attacks" => $attacks, "phases" => $phases, "citys" => $citys);
        self::renderView("Crear Others", "Others/crear",$vars);
    }

    public function save($class){
 
        if($class == "City"){
            $obj = new City(null,$_POST["name"]);
        }else if($class == "Type"){
            $effective = Type::get($_POST["effective"]);
            $weak = Type::get($_POST["weak"]);
            $obj = new Type(null,$_POST["name"],$_POST["effective"],$_POST["weak"]);
            $obj->has_one("effective", $effective);
            $obj->has_one("weak",$weak);
        }else if($class == "Sex"){
            $obj = new Sex(null,$_POST["name"]);
        }else if($class == "Phase"){
            $obj = new Phase(null,$_POST["name"]);
        }else if($class == "Attack"){
            $type = Type::get($_POST["Type_id"]);
            $obj = new Attack(null,$_POST["name"],$_POST["description"],$_POST["Type_id"],$_POST["pp"],$_POST["pd"]);
            $obj->has_one("type",$type);
        }

        $obj->create();

        //self::crear();
        header("LOCATION: "._URL."Others/crear");
    }

    public function update($class){

        if($class == "City"){
            $obj = new City($_POST["id"],$_POST["name"]);
        }else if($class == "Type"){
            $obj = new Type($_POST["id"],$_POST["name"],$_POST["effective"],$_POST["weak"]);
        }else if($class == "Sex"){
            $obj = new Sex($_POST["id"],$_POST["name"]);
        }else if($class == "Phase"){
            $obj = new Phase($_POST["id"],$_POST["name"]);
        }else if($class == "Attack"){
            $obj = new Attack($_POST["id"],$_POST["name"],$_POST["description"],$_POST["Type_id"],$_POST["pp"],$_POST["pd"]);
        }

        $obj->update();
        //self::crear();
        header("LOCATION: "._URL."Others/crear");
    }

    public function delete($class){

        print_r($class);
        print_r($_POST["id"]);
        $obj = $class::get($_POST["id"]);
        $obj->delete();
        //self::crear();
        header("LOCATION: "._URL."Others/crear");
    }


}
OthersCtrlr::checkRequest($url,'OthersCtrlr');