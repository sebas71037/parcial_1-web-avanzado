<?php

class RegisterCtrlr extends Controller{

    public function crear(){
        $cities = City::getAll();
        $sexs = Sex::getAll();
        $types = Type::getAll();
        $attacks = Attack::getAll();
        $vars = array("cities"=>$cities,"sexs"=>$sexs,"types"=>$types,"attacks"=>$attacks);
        self::renderView("Crear Entrenador", "Register/crear",$vars);
    }

    public function objectNullifier($Pokemons){

        foreach ($Pokemons as $key => $value) {

            foreach ($value as $key2 => $value2) {
                if($value->$key2 === '') $value->$key2 = null;
            }
            foreach ($value->Attack as $key2 => $value2) {
                if($value->Attack[$key2] === '') $value->Attack[$key2] = null;
                
            }

            foreach ($value->Type as $key2 => $value2) {
                if($value->Type[$key2] === '') $value->Type[$key2] = null;
                
            }
        }        
    }

    public function save(){
 
        $dataTrainer = get_object_vars(json_decode($_POST["pepe"]));
        $Pokemons = json_decode($_POST["pepe2"]);
        self::objectNullifier($Pokemons);
        $trainer = new Trainer($dataTrainer["id"],$dataTrainer["name"],$dataTrainer["second_name"],
                                    $dataTrainer["City_id"],$dataTrainer["age"],
                                    $dataTrainer["photo"],$dataTrainer["Sex_id"]);
        $city = City::get($dataTrainer["City_id"]);
        $sex = Sex::get($dataTrainer["Sex_id"]);
        $trainer->has_one("city",$city);
        $trainer->has_one("sex",$sex);
        $trainer->create();

        foreach ($Pokemons as $key => $value) {
            //$type = Type::get($value->Type_id1);
            //$attack = Attack::getByName("Llamarada");//falta implementar attacks con ajax
            $sex = Sex::get($value->Sex_id);
            $trainer = Trainer::get($dataTrainer["id"]);
            $pokemon = new Pokemon(null,$value->name, $value->weight, $value->Sex_id, $value->lp, $value->invo, 
                                   $value->evo, $value->mega, $value->photo, $dataTrainer["id"], $value->principal);
            $pokemon->has_one("sex",$sex);
            $pokemon->has_one("trainer", $trainer);
            $pokemon->create();
            $pokemon->setLP($pokemon->lpt);

            foreach ($value->Type as $key2 => $value2) {
                if( $value2 != "null" ){
                    $type = Type::get($value2);
                    $pokemon->has_many("type",$type);
                }
            }
            $i=0;
            foreach ($value->Attack as $key2 => $value2) {
                if( $value2 != "null" ){
                    $attack = Attack::get($value2);
                    $pokemon->has_many("attack",$attack);
                    $pokemon->has_many["attack"]["relationships"][$i]["pp"] = $attack->ppt;
                    $i++;
                }
            }
            $pokemon->update();
        }



        self::crear();
    }

    function checkId(){

        $value = $_POST;
        $trainers = Trainer::getAll();
        $banderaId = 0;

        foreach ($trainers as $key => $trainer) {
            
            if(($trainer['id'] === $value['id'])){ $banderaId = 1; break;}
            else $banderaId = 0;
        }
        echo $banderaId;
    }

    public function update(){
        $trainer = new Trainer($_POST["id"],$_POST["name"],$_POST["second_name"],
                                    $_POST["City_id"],$_POST["age"],
                                    "null",$_POST["Sex_id"]);
        $trainer->update();
        self::crear();
    }

    public function delete(){
        $trainer = Trainer::getByName($_POST["name"]);
        $trainer->delete();
        self::crear();
    }
}
RegisterCtrlr::checkRequest($url,'RegisterCtrlr');




/*if(isset($url[1]) && $url[1] != ''){
    $url[1]();
}
    
    function crear(){
        $cities = City::getAll();
        $sexs = Sex::getAll();
        $types = Type::getAll();
        $vars = array("cities"=>$cities,"sexs"=>$sexs,"types"=>$types);
        require _VIEWS.'Trainer/crear.php';
    }

    function save(){
        $trainer = new Trainer($_POST["id"],$_POST["name"],$_POST["second_name"],
                                    $_POST["City_id"],$_POST["age"],
                                    "null",$_POST["Sex_id"]);
        $city = City::get($_POST["City_id"]);
        $sex = Sex::get($_POST["Sex_id"]);
        $trainer->has_one("city",$city);
        $trainer->has_one("sex",$sex);
        $trainer->create();       
        crear();
    }
*/