<?php

class Attack extends ORM{

	protected static $table = "attack";
	public $id,$name,$description,$Type_id,$ppt,$pd;

    public $has_one = array(
    		'type'=>array(
    			'class'=>'Type',
	            'join_as'=>'Type_id',
	            'join_with'=>'id',
	            'fkey_table'=>'type'
    		));
                
	function __construct($id,$name,$description,$Type_id,$ppt,$pd){

		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
		$this->Type_id = $Type_id;
		$this->ppt = $ppt;
        $this->pd = $pd;
	}

	function get( $id ){
		$data = array_shift(self::where("id",$id));
		$usr = new self($data["id"],$data["name"],$data["description"],$data["Type_id"],$data["ppt"],$data["pd"]);
		return $usr;
	}

	function getByName( $name ){
		$data = array_shift(self::where("name",$name));
		//Logger::debug("data",$data,"getByName");
		$usr = new self($data["id"],$data["name"],$data["description"],$data["Type_id"],$data["ppt"],$data["pd"]);
		return $usr;
	}

}