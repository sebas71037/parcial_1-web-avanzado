<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Smash
 *
 * @author usuario
 */
class Smash extends ORM{
    
    protected static $table = "smash";
    
    /**
    Funcion de batalla entre entrenadores
    */
    public function  fight($id_A,$id_B,$pokemons_A,$pokemons_B){
    	$winner = 0;
    	$looser = 0;
    	$turns = 0;
    	$attacks_used = array();

    	while(count($pokemons_A) > 0 && count($pokemons_B) > 0){
    		$index_A = rand(0,count($pokemons_A)-1);
    		$index_B = rand(0,count($pokemons_B)-1);
    		$pokemon_A = Pokemon::get($pokemons_A[$index_A]["id"]);
    		$pokemon_B = Pokemon::get($pokemons_B[$index_B]["id"]);
    		$result = self::fight_pokemon($pokemon_A,$pokemon_B,$index_A,$index_B);
    		if($index_A == $result["Perdedor"]){
    			self::quitPokemon($pokemons_A, $index_A);
    		}elseif ($index_B == $result["Perdedor"]) {
    			self::quitPokemon($pokemons_B, $index_B);
    		}
    		$turns += $result["Turnos"];
    		$attacks_used["Pokemon_Fight_".(count($attacks_used)+1)] = $result["Attacks_Used"];
    	}

    	if(count($pokemons_A) == 0){
    		$winner = $id_B;
    		$looser = $id_A;
    	}else if(count($pokemons_B) == 0){
    		$winner = $id_A;
    		$looser = $id_B;
    	}

    	$results = array("winner"=>$winner,"looser"=>$looser,"turnos"=>$turns,"attacks_used"=>$attacks_used);
    	return $results;
    }
/**
Saca un pokemon del arreglo de pokemones de cierto entrenador durante un encuentro, es decir, cuando ese pokemon
  pierde la pelea
*/
    public function quitPokemon(&$pokemons, $index){
    	unset($pokemons[$index]);
    	$pokemons = array_values($pokemons);
    }
/**
Coge los pokemones de un entrenador y deja solo los que tengan life points(lp) para combatir
*/
    public function pokemonsReadyFight(&$pokemons){
    	$posiciones = array();
		for ($i=0; $i < count($pokemons); $i++) { 
			if($pokemons[$i]["lp"] == 0){
				$posiciones[] = $i;
			}
		}

		for ($i=0; $i < count($posiciones); $i++) { 
			unset($pokemons[$posiciones[$i]]);
			$pokemons = array_values($pokemons);
		}
    }

/**
Genera las estadisticas de un enfrentamiento entre entrenadores (function que hace todo)
**/
    public function staticFight($id_A,$id_B,$phase){
  
    	$pokemons_smashed = array();
    	$pokemons_A =  Pokemon::where("Trainer_id",$id_A);
    	self::pokemonsReadyFight($pokemons_A);
    	foreach ($pokemons_A as $key => $value) {
    		$pokemons_smashed["pokemons_A"][] = $value["id"];
    	}
    	$pokemons_B =  Pokemon::where("Trainer_id",$id_B);
    	self::pokemonsReadyFight($pokemons_B);
    	foreach ($pokemons_B as $key => $value) {
    		$pokemons_smashed["pokemons_B"][] = $value["id"];
    	}

    	$results = self::fight($id_A,$id_B,$pokemons_A,$pokemons_B);
    	if($id_A == $results["winner"]){
    		self::restorePP($pokemons_smashed["pokemons_A"]);
    	}else if($id_B == $results["winner"]){
    		self::restorePP($pokemons_smashed["pokemons_B"]);
    	}

        $static = array(
                                "id_A"=> $id_A,
                                "id_B"=> $id_B,
                                "pokemons_smashed"=> $pokemons_smashed,
                                "attacks_used"=> $results["attacks_used"],
                                "turns"=> $results["turnos"],
                                "winner"=> $results["winner"],
                                "looser"=> $results["looser"],
                                "phase"=> $phase
                                 );

        return $static;
    }
/**
 restaurara los pokemones del entrenador ganador
 **/
    public function restorePP($pokemons_smashed){
    	foreach ($pokemons_smashed as $key => $value) {
    		$attacks = ORM::whereR("Attack_id","Pokemon_id",$value,"pokemon_has_attack");
    		foreach ($attacks as $key2 => $value2) {
    			$attack = Attack::get($value2["Attack_id"]);
    			$values = array('Pokemon_id'=>$value,'Attack_id'=>$value2["Attack_id"],'pp'=>$attack->ppt);
	    		$result = ORM::updateTI('pokemon_has_attack',$values,'Pokemon_id = '.$value.' AND Attack_id = '.$value2["Attack_id"]);
    		}
    	}	
    }
/**
Combate de pokemon con pokemon
**/
    public function fight_pokemon( $pokemon_A, $pokemon_B, $index_A, $index_B){
    	$value = 0;
    	$turns = 0;
    	$winner = "";
    	$looser = "";
    	$attacks_used = array("Attacks_A"=>array(),"Attacks_B"=>array());

    	//Obtiene los ids de los ataques de un pokemon en especifico
    	$attacks_id_A = ORM::whereR("Attack_id","Pokemon_id",$pokemon_A->id,"pokemon_has_attack");
    	$attacks_id_B = ORM::whereR("Attack_id","Pokemon_id",$pokemon_B->id,"pokemon_has_attack");

    	//Obtiene los ataques como tal
    	for ($i = 0; $i < count($attacks_id_A); $i++) { 
    		$attacks_A[] = Attack::get($attacks_id_A[$i]["Attack_id"]);
    		$type_A[] =  Type::get($attacks_A[$i]->Type_id);
    	}

    	for ($i = 0; $i < count($attacks_id_B); $i++) { 
    		$attacks_B[] = Attack::get($attacks_id_B[$i]["Attack_id"]);
    		$type_B[] =  Type::get($attacks_B[$i]->Type_id);
    	}

    	//Arreglo que contendra los poderes que pierdan sus puntos de poder(pp)
    	$vacios = array("A"=>array(),"B"=>array());

    	//Ciclo de la pelea completa hasta que 1 pokemon pierda su Point lifes o que se quede sin puntos de poder (pp)
    	while ($pokemon_A->lp > 0 && $pokemon_B->lp > 0) {
    		$randomA = -1;
    		$randomB = -1;
    		$result_void_A = self::detect_void($randomA, $vacios, $pokemon_A,$pokemon_B,array('A'=>$attacks_A,'B'=>$attacks_B),'A');
    		if($result_void_A === "finish"){
    			print_r("Se han acabado los poderes del pokemon A <br>");
    			$winner = $index_B;
    			$looser = $index_A;
    			$result = self::updatePokemonsFight($pokemon_A,$pokemon_B,$attacks_A,$attacks_B,$turns,$winner,$looser,$attacks_used);
    			return $result;
    		}
    		$result_void_B = self::detect_void($randomB, $vacios, $pokemon_A,$pokemon_B,array('A'=>$attacks_A,'B'=>$attacks_B),'B');
    		if($result_void_B === "finish"){
    			$winner = $index_A;
    			$looser = $index_B;
    			print_r("Se han acabado los poderes del pokemon B <br>");
    			$result = self::updatePokemonsFight($pokemon_A,$pokemon_B,$attacks_A,$attacks_B,$turns,$winner,$looser,$attacks_used);
    			return $result;
    		}

    			$pd_A = $attacks_A[$result_void_A]->pd;
    			if(!in_array($attacks_A[$result_void_A]->id, $attacks_used["Attacks_A"])) $attacks_used["Attacks_A"][] = $attacks_A[$result_void_A]->id;
    			$pd_B = $attacks_B[$result_void_B]->pd;
    			if(!in_array($attacks_B[$result_void_B]->id, $attacks_used["Attacks_B"])) $attacks_used["Attacks_B"][] = $attacks_B[$result_void_B]->id;
    		//$value seria el condicional que dice que pokemon atacara en el siguiente turno
    		if($value == 0 ){
    			if($attacks_A[$result_void_A]->ppt > 0){
	    			if($type_A[$result_void_A]->id == $type_B[$result_void_B]->weak){
	    				$pd_A = $pd_A*2;
	    			}else if($type_A[$result_void_A]->id == $type_B[$result_void_B]->effective){
	    				$pd_A = $pd_A*0.5;	
	    			}	
	    			$pokemon_B->lp = $pokemon_B->lp - $pd_A;
	    			$attacks_A[$result_void_A]->ppt =  $attacks_A[$result_void_A]->ppt - 1;
	    			$value = 1;
	    			$turns += 1;
	    		}
    		}else if($value == 1 && $pokemon_A->lp > 0 && $pokemon_B->lp > 0){
    			if($attacks_B[$result_void_B]->ppt > 0){
	    			if($type_B[$result_void_B]->id == $type_A[$result_void_A]->weak){
	    				$pd_B = $pd_B*2;
	    			}else if($type_B[$result_void_B]->id == $type_A[$result_void_A]->effective){
	    				$pd_B = $pd_B*0.5;	
	    			}		
	    			$pokemon_A->lp = $pokemon_A->lp - $pd_B;
	    			$attacks_B[$result_void_B]->ppt =  $attacks_B[$result_void_B]->ppt - 1;
	    			$value = 0;
	    			$turns += 1;
	    		}
    		}
    	}
    	if($pokemon_A->lp <= 0){
    		$winner = $index_B;
    		$looser = $index_A;
    	}else if($pokemon_B->lp <= 0){
    		$winner = $index_A;
    		$looser = $index_B;
    	}
    	$result = self::updatePokemonsFight($pokemon_A,$pokemon_B,$attacks_A,$attacks_B,$turns,$winner,$looser,$attacks_used);

    	return $result;
    }

/**
Detección de un ataque que llega a tener los puntos de poder en 0, en el caso de que de que un pokemon termine
con todos sus ataques, es decir, que queden con puntos de poder en 0, retornara el final de la pelea y si no sucede
ninguna de las condiciones anteriores retorna un poder aleatorio para ser usado en el turno correspondiente
por dicho pokemon
*/
    public function detect_void($random, &$vacios, $pokemon_A,$pokemon_B,$attacks,$index){
    	    while ($random == -1) {
    			$random = rand(0,count($attacks[$index])-1);
    			if(!in_array($random, $vacios[$index]) && $attacks[$index][$random]->ppt == 0){
    				$vacios[$index][] = $random;
    				$random = -1;
    			}
    			if(count($vacios[$index]) == count($attacks)){
    			  	$result = "finish";
    				return $result;
    			}
    		}
    		return $random;
    }

/**
Actualiza la vida de los pokemones y los pp de sus poderes
**/
    public function updatePokemonsFight($pokemon_A,$pokemon_B,$attacks_A,$attacks_B,$turns,$winner,$looser,$attacks_used){
    	if($pokemon_A->lp < 0) $pokemon_A->lp = 0;
    	foreach ($attacks_A as $key => $value) {
	    	$values = array('Pokemon_id'=>$pokemon_A->id,'Attack_id'=>$value->id,'pp'=>$value->ppt);
	    	$result = ORM::updateTI('pokemon_has_attack',$values,'Pokemon_id = '.$values['Pokemon_id'].' AND Attack_id = '.$values['Attack_id']);
    	}
    	$pokemon_A->update();
    	if($pokemon_B->lp < 0) $pokemon_B->lp = 0;
    	foreach ($attacks_B as $key => $value) {
	    	$values = array('Pokemon_id'=>$pokemon_B->id,'Attack_id'=>$value->id,'pp'=>$value->ppt);
	    	$result = ORM::updateTI('pokemon_has_attack',$values,'Pokemon_id = '.$values['Pokemon_id'].' AND Attack_id = '.$values['Attack_id']);
    	}
    	$pokemon_B->update();

    	$result = array("pokemon_A"=> $pokemon_A->lp, "pokemon_B"=>$pokemon_B->lp,"Turnos"=>$turns,"Ganador"=>$winner,"Perdedor"=>$looser,"Attacks_Used"=>$attacks_used);
    	return $result;
    }

}
