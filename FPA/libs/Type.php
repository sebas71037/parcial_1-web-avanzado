<?php

class Type extends ORM{

	protected static $table ="type";
	public $id,$type,$effective,$weak;
        
    public $has_one = array(
    		'effective'=>array(
    			'class'=>'Type',
	            'join_as'=>'effective',
	            'join_with'=>'id',
	            'fkey_table'=>'type'
    		),
            'weak'=>array(
    			'class'=>'Type',
	            'join_as'=>'weak',
	            'join_with'=>'id',
	            'fkey_table'=>'type'
    		)
    	);

	function __construct($id,$type,$effective = null,$weak = null){
		$this->id = $id;
		$this->type = $type;
        $this->effective = $effective;
        $this->weak = $weak;

	}
	//Escribir mètodo en ORM y sobreescribirlo en las clases
	function get( $id ){
		$data = array_shift(self::where("id",$id));
		$type = new self($data["id"],$data["type"],$data["effective"],$data["weak"]);
		return $type;
	}

	function getByName( $type ){
		$types = array_shift(self::where("type",$type));
		//Logger::debug("data",$data,"getByName");
		$type = new self($types["id"],$types["type"],$types["effective"],$types["weak"]);
		return $type;
	}


}