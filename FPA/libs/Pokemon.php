<?php

class Pokemon extends ORM{
    protected static $table = "pokemon";
    public $id, $name, $weight, $Sex_id, $lpt, $invo, $evo, $mega, $photo, $Trainer_id, $principal,$lp;

   public $has_many = array(
        'type'=>array(
            'class'=>'Type',
            'join_self_as'=>'Pokemon_id',
            'join_other_as'=>'Type_id',
            'join_table'=>'pokemon_has_type'
            ),
        'attack'=> array(
            'class'=>'Attack',
            'join_self_as'=>'Pokemon_id',
            'join_other_as'=>'Attack_id',
            'join_table'=>'pokemon_has_attack'
            )
        );

    public $has_one = array(
        'sex'=>array(
                'class'=>'Sex',
                'join_as'=>'Sex_id',
                'join_with'=>'id',
                'fkey_table'=>'sex'
            ),
        'trainer'=>array(
                'class'=>'Trainer',
                'join_as'=>'Trainer_id',
                'join_with'=>'id',
                'fkey_table'=>'trainer'
            ),
        'invo'=>array(
                'class'=>'Pokemon',
                'join_as'=>'id',
                'join_with'=>'invo',
                'fkey_table'=>'Pokemon'
            ),
        'evo'=>array(
                'class'=>'Pokemon',
                'join_as'=>'id',
                'join_with'=>'evo',
                'fkey_table'=>'Pokemon'
            ),
        'mega'=>array(
                'class'=>'Pokemon',
                'join_as'=>'id',
                'join_with'=>'mega',
                'fkey_table'=>'Pokemon'
            )
        );
                
    function __construct($id, $name, $weight, $Sex_id, $lpt, $invo, $evo, $mega, $photo, $Trainer_id, $principal){

        $this->id = $id;
        $this->name = $name;
        $this->weight = $weight;
        $this->Sex_id = $Sex_id;
        $this->lpt = $lpt;
        $this->invo = $invo;
        $this->evo = $evo;
        $this->mega = $mega;
        $this->photo = $photo;
        $this->Trainer_id = $Trainer_id;
        $this->principal = $principal;
    }

    function get( $id ){
        $data = array_shift(self::where("id",$id));
        $usr = new self($data["id"], $data["name"], $data["weight"], $data["Sex_id"], $data["lpt"], $data["invo"], $data["evo"], $data["mega"], $data["photo"], $data["Trainer_id"], $data["principal"]);
        $usr->setLP($data["lp"]);
        return $usr;
    }

    function setLP( $val ){
        $this->lp = $val;
    }

    function getByName( $name ){
        $data = array_shift(self::where("name", $name));
        $usr = new self($data["id"], $data["name"], $data["weight"], $data["Sex_id"], $data["lpt"], $data["invo"], $data["evo"], $data["mega"], $data["photo"], $data["Trainer_id"], $data["principal"]);
        $usr->setLP($data["lp"]);
        return $usr;
    }
}
