<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Entrenador
 *
 * @author usuario
 */
class Trainer extends ORM{
    protected static $table = "trainer";
    public $id,$name,$second_name,$City_id,$age,$photo,$Sex_id;

    public $has_many = array();

    public $has_one = array(
    	'city'=>array(
    		    'class'=>'City',
	            'join_as'=>'City_id',
	            'join_with'=>'id',
	            'fkey_table'=>'city'
    		),
        'sex'=>array(
                'class'=>'Sex',
                'join_as'=>'Sex_id',
                'join_with'=>'id',
                'fkey_table'=>'sex'
            )
    	);
                
	function __construct($id,$name,$second_name,$City_id,$age,$photo,$Sex_id){
		$this->id = $id;
		$this->name = $name;
		$this->second_name = $second_name;
        $this->City_id = $City_id;
        $this->age = $age;
        $this->photo = $photo;
        $this->Sex_id = $Sex_id;
	}

    function get( $id ){
        $data = array_shift(self::where("id",$id));
        $usr = new self($data["id"],$data["name"],$data["second_name"],$data["City_id"],$data["age"],$data["photo"],$data["Sex_id"]);
        return $usr;
    }

    function getByName( $trainer ){
        $data = array_shift(self::where("name",$trainer));
        $usr = new self($data["id"],$data["name"],$data["second_name"],$data["City_id"],$data["age"],$data["photo"],$data["Sex_id"]);
        return $usr;
    }
}
