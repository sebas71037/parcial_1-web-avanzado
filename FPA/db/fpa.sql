-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema fpa
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `fpa` ;

-- -----------------------------------------------------
-- Schema fpa
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `fpa` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `fpa` ;

-- -----------------------------------------------------
-- Table `fpa`.`type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`type` ;

CREATE TABLE IF NOT EXISTS `fpa`.`type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `type` VARCHAR(45) NOT NULL COMMENT '',
  `effective` INT(11) NULL DEFAULT NULL COMMENT '',
  `weak` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `debil_idx` (`weak` ASC)  COMMENT '',
  INDEX `efectivo_idx` (`effective` ASC)  COMMENT '',
  CONSTRAINT `debil`
    FOREIGN KEY (`weak`)
    REFERENCES `fpa`.`type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `efectivo`
    FOREIGN KEY (`effective`)
    REFERENCES `fpa`.`type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`attack`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`attack` ;

CREATE TABLE IF NOT EXISTS `fpa`.`attack` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(45) NOT NULL COMMENT '',
  `description` LONGTEXT NOT NULL COMMENT '',
  `Type_id` INT(11) NOT NULL COMMENT '',
  `ppt` INT(11) NOT NULL DEFAULT '1' COMMENT '',
  `pd` INT(11) NOT NULL DEFAULT '1' COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `idTipo_idx` (`Type_id` ASC)  COMMENT '',
  CONSTRAINT `tipo`
    FOREIGN KEY (`Type_id`)
    REFERENCES `fpa`.`type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`phase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`phase` ;

CREATE TABLE IF NOT EXISTS `fpa`.`phase` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `phase` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`sex`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`sex` ;

CREATE TABLE IF NOT EXISTS `fpa`.`sex` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `sex` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`city`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`city` ;

CREATE TABLE IF NOT EXISTS `fpa`.`city` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `city` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`trainer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`trainer` ;

CREATE TABLE IF NOT EXISTS `fpa`.`trainer` (
  `id` INT(11) NOT NULL COMMENT '',
  `name` VARCHAR(45) NOT NULL COMMENT '',
  `second_name` VARCHAR(45) NOT NULL COMMENT '',
  `City_id` INT(11) NOT NULL COMMENT '',
  `age` INT(11) NOT NULL COMMENT '',
  `photo` VARCHAR(150) NOT NULL COMMENT '',
  `Sex_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `idSexo_idx` (`Sex_id` ASC)  COMMENT '',
  INDEX `city_idx` (`City_id` ASC)  COMMENT '',
  CONSTRAINT `sexo`
    FOREIGN KEY (`Sex_id`)
    REFERENCES `fpa`.`sex` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `city`
    FOREIGN KEY (`City_id`)
    REFERENCES `fpa`.`city` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`pokemon`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`pokemon` ;

CREATE TABLE IF NOT EXISTS `fpa`.`pokemon` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(45) NOT NULL COMMENT '',
  `weight` INT(11) NOT NULL COMMENT '',
  `Sex_id` INT(11) NOT NULL COMMENT '',
  `lpt` INT(11) NOT NULL COMMENT '',
  `invo` INT(11) NULL DEFAULT NULL COMMENT '',
  `evo` INT(11) NULL DEFAULT NULL COMMENT '',
  `mega` INT(11) NULL DEFAULT NULL COMMENT '',
  `photo` VARCHAR(150) NOT NULL COMMENT '',
  `Trainer_id` INT(11) NOT NULL COMMENT '',
  `principal` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '',
  `lp` INT(11) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `Pokemon_PK_Sexo_idx` (`Sex_id` ASC)  COMMENT '',
  INDEX `Pokemon_FK_Entrenador_idx` (`Trainer_id` ASC)  COMMENT '',
  INDEX `Pokemon_FK_Pokemon_prevo_idx` (`invo` ASC)  COMMENT '',
  INDEX `Pokemon_FK_Pokemon_evo_idx` (`evo` ASC)  COMMENT '',
  INDEX `Pokemon_FK_Pokemon_mega_idx` (`mega` ASC)  COMMENT '',
  CONSTRAINT `Pokemon_FK_Entrenador`
    FOREIGN KEY (`Trainer_id`)
    REFERENCES `fpa`.`trainer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Pokemon_FK_Pokemon_evo`
    FOREIGN KEY (`evo`)
    REFERENCES `fpa`.`pokemon` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Pokemon_FK_Pokemon_mega`
    FOREIGN KEY (`mega`)
    REFERENCES `fpa`.`pokemon` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Pokemon_FK_Pokemon_prevo`
    FOREIGN KEY (`invo`)
    REFERENCES `fpa`.`pokemon` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Pokemon_FK_Sexo`
    FOREIGN KEY (`Sex_id`)
    REFERENCES `fpa`.`sex` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`pokemon_has_attack`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`pokemon_has_attack` ;

CREATE TABLE IF NOT EXISTS `fpa`.`pokemon_has_attack` (
  `Pokemon_id` INT(11) NOT NULL COMMENT '',
  `Attack_id` INT(11) NOT NULL COMMENT '',
  `pp` INT(11) NULL COMMENT '',
  PRIMARY KEY (`Pokemon_id`, `Attack_id`)  COMMENT '',
  INDEX `idAtaque_idx` (`Attack_id` ASC)  COMMENT '',
  CONSTRAINT `Pokeataque_FK_Ataque`
    FOREIGN KEY (`Attack_id`)
    REFERENCES `fpa`.`attack` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Pokeataque_FK_Pokemon`
    FOREIGN KEY (`Pokemon_id`)
    REFERENCES `fpa`.`pokemon` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`pokemon_has_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`pokemon_has_type` ;

CREATE TABLE IF NOT EXISTS `fpa`.`pokemon_has_type` (
  `Pokemon_id` INT(11) NOT NULL COMMENT '',
  `Type_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`Pokemon_id`, `Type_id`)  COMMENT '',
  INDEX `fk_Pokemon_has_Tipo_Tipo1_idx` (`Type_id` ASC)  COMMENT '',
  INDEX `fk_Pokemon_has_Tipo_Pokemon1_idx` (`Pokemon_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Pokemon_has_Tipo_Pokemon1`
    FOREIGN KEY (`Pokemon_id`)
    REFERENCES `fpa`.`pokemon` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Pokemon_has_Tipo_Tipo1`
    FOREIGN KEY (`Type_id`)
    REFERENCES `fpa`.`type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fpa`.`smash`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fpa`.`smash` ;

CREATE TABLE IF NOT EXISTS `fpa`.`smash` (
  `Trainer_id_A` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `Trainer_id_B` INT(11) NOT NULL COMMENT '',
  `Phase_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`Trainer_id_A`, `Trainer_id_B`)  COMMENT '',
  INDEX `idEntrenadorB_idx` (`Trainer_id_B` ASC)  COMMENT '',
  INDEX `idEtapa_idx` (`Phase_id` ASC)  COMMENT '',
  CONSTRAINT `etapa`
    FOREIGN KEY (`Phase_id`)
    REFERENCES `fpa`.`phase` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `idEntrenadorA`
    FOREIGN KEY (`Trainer_id_A`)
    REFERENCES `fpa`.`trainer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `idEntrenadorB`
    FOREIGN KEY (`Trainer_id_B`)
    REFERENCES `fpa`.`trainer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Data for table `fpa`.`type`
-- -----------------------------------------------------
START TRANSACTION;
USE `fpa`;
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (1, 'Fuego', 4, 2);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (2, 'Agua', 1, 4);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (3, 'Acero', 10, 16);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (4, 'Planta', 2, 1);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (5, 'Bicho', 13, 18);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (6, 'Dragón', 6, 9);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (7, 'Electrico', 18, 16);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (8, 'Fantasma', 8, 15);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (9, 'Hada', 11, 17);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (10, 'Hielo', 16, 14);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (11, 'Lucha', 12, 9);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (12, 'Normal', NULL, 11);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (13, 'Psiquico', 17, 5);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (14, 'Roca', 10, 4);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (15, 'Siniestro', 8, 9);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (16, 'Tierra', 7, 2);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (17, 'Veneno', 4, 13);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (18, 'Volador', 5, 7);
INSERT INTO `fpa`.`type` (`id`, `type`, `effective`, `weak`) VALUES (19, 'Desconocido', NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `fpa`.`phase`
-- -----------------------------------------------------
START TRANSACTION;
USE `fpa`;
INSERT INTO `fpa`.`phase` (`id`, `phase`) VALUES (DEFAULT, 'Fase de grupos');
INSERT INTO `fpa`.`phase` (`id`, `phase`) VALUES (DEFAULT, 'Octavos');
INSERT INTO `fpa`.`phase` (`id`, `phase`) VALUES (DEFAULT, 'Cuartos');
INSERT INTO `fpa`.`phase` (`id`, `phase`) VALUES (DEFAULT, 'Semifinal');
INSERT INTO `fpa`.`phase` (`id`, `phase`) VALUES (DEFAULT, 'Final');

COMMIT;


-- -----------------------------------------------------
-- Data for table `fpa`.`sex`
-- -----------------------------------------------------
START TRANSACTION;
USE `fpa`;
INSERT INTO `fpa`.`sex` (`id`, `sex`) VALUES (DEFAULT, 'M');
INSERT INTO `fpa`.`sex` (`id`, `sex`) VALUES (DEFAULT, 'F');
INSERT INTO `fpa`.`sex` (`id`, `sex`) VALUES (DEFAULT, 'U');
INSERT INTO `fpa`.`sex` (`id`, `sex`) VALUES (DEFAULT, 'FM');

COMMIT;


-- -----------------------------------------------------
-- Data for table `fpa`.`city`
-- -----------------------------------------------------
START TRANSACTION;
USE `fpa`;
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (1, 'Pueblo Paleta');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (2, 'Ciudad Verde');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (3, 'Pueblo Lavanda');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (4, 'Pueblo Primavera');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (5, 'Ciudad Cerezo');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (6, 'Ciudad Malva');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (7, 'Villa Raiz');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (8, 'Pueblo Escaso');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (9, 'Ciudad Petalia');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (10, 'Pueblo Hojaverde');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (11, 'Pueblo Arena');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (12, 'Ciudad Jubileo');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (13, 'Pueblo Arcilla');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (14, 'Pueblo Terracota');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (15, 'Ciudad Gres');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (16, 'Pueblo Boceto');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (17, 'Pueblo Acuarela');
INSERT INTO `fpa`.`city` (`id`, `city`) VALUES (18, 'Ciudad Luminalia');

COMMIT;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
