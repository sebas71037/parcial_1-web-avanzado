<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Pokemon Ligue</title>
	<link rel="stylesheet" type="text/css" href="../<?php echo _CSS; ?>style.css">
	<script src="../<?php echo _JS; ?>jquery.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oxygen:700' rel='stylesheet' type='text/css'>
</head>
<body>
	<div id="container">
		<header>
			<div id="firstHeaderCont">
				<div id="imgHeaderCont">
					<!--img src="Charizard.png" alt=""--><h1>
					POKEMON</h1>
				</div>
			</div>
			<nav>
				<div id="navInsideCont">
					<ul id="principal">
						<li><a href ="<?php echo _URL; ?>index">INDEX</a></li><li id="registro">
						<a href     ="">REGISTER</a><ul id="alternative"><li>
						<a href     ="<?php echo _URL; ?>Register/crear">LIGUE</a></li><li>
						<a href     ="crear">OTHERS</a></li></ul></li>
						<li><a href     ="">COLISSEUM</a></li>
					</ul>
				</div>
			</nav>
		</header>
		<!--div id="imgBack"></div-->
		<section id="introductionSection">
			<div id="registerTitle">
				<h2>Welcome Trainer</h2>
				<h3>Extras</h3>
			</div>
		</section>

		<section id="formSection">
			<div id="contForm">
				<div id="contHideForm">
					<form id="typeForm" name="Type" method="POST" action="save/Type" target="" enctype="">
						<div class="formCont">
							<h3>Tipo</h3>
							<p>En esta apartado puede editar, crear y eliminar tipos</p>
						</div>
						<div class="formCont">
							<select name ="id">
								<option value="">Seleccione un ID</option>
								<?php 
									foreach ($vars["types"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
									}
								?>
							</select>
							<input name ="name" placeholder="Tipo" type="text">	
							<select name ="effective" >
								<option value="">Seleccione una efectividad</option>
								<?php 
									foreach ($vars["types"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
									}
								?>
							</select>
							<select name ="weak">
								<option value="">Seleccione una debilidad</option>
								<?php 
									foreach ($vars["types"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
									}
								?>
							</select>
						</div>
						<div class="formCont">
							<input name="" type="submit" value="Dale">
						</div>
						<!--input name="" type="submit" value="ENVIAR"-->
					</form>
					<form id="sexForm" name="Sex" method="POST" action="save/Sex" target="" enctype="">
						<div class="formCont">
							<h3>Sexo</h3>
							<p>En esta apartado puede editar, crear y eliminar sexos</p>
						</div>
						<div class="formCont">
							<select name ="id">
								<option value="">Seleccione un ID</option>
								<?php 
									foreach ($vars["sexs"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["sex"]."</option>";
									}
								?>
							</select>	
							<input name ="name" placeholder="Sexo" type="text"  maxlength="2">
						</div>
						<div class="formCont">
							<input name="" type="submit" value="Dale">
						</div>
					</form>
					<form id="phaseForm" name="Phase" method="POST" action="save/Phase" target="" enctype="">
						<div class="formCont">
							<h3>Fase</h3>
							<p>En esta apartado puede editar, crear y eliminar fases</p>
						</div>
						<div class="formCont">
							<select name ="id">
									<option value="">Seleccione un ID</option>
									<?php 
										foreach ($vars["types"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
										}
									?>
							</select>	
							<input name ="name" placeholder="Fase" type="text" >
						</div>
						<div class="formCont">
							<input name="" type="submit" value="Dale">
						</div>
					</form>
					<form id="cityForm" name="City" method="POST" action="save/City" target="" enctype="">
						<div class="formCont">
							<h3>Ciudad</h3>
							<p>En esta apartado puede editar, crear y eliminar ciudades</p>
						</div>
						<div class="formCont">
							<select name ="id">
									<option value="">Seleccione un ID</option>
									<?php 
										foreach ($vars["citys"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["city"]."</option>";
										}
									?>
							</select>
							<input name ="name" placeholder="Ciudad" type="text" required>	
						</div>
						<div class="formCont">
							<input name="" type="submit" value="Dale">
						</div>
					</form>
					<form id="ataqueForm" name="Attack" method="POST" action="save/Attack" target="" enctype="">
						<div class="formCont">
							<h3>Ataque</h3>
							<p>En esta apartado puede editar, crear y eliminar ataques</p>
						</div>
						<div class="formCont">
							<select name ="id">
								<option value="">Seleccione un ID</option>
								<?php 
									foreach ($vars["attacks"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["name"]."</option>";
									}
								?>
							</select>
							<input name ="name" placeholder="Nombre" type="text" required>							
							<select name ="Type_id" required>
								<option value="" name="id">Seleccione un tipo</option>
								<?php 
									foreach ($vars["types"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
									}
								?>	
							</select>		
							<input name ="pp" placeholder="Puntos de poder" type="text" required>
							<input name ="pd" placeholder="Puntos de ataque" type="text" required>	
							<textarea name ="description" rows="4" required></textarea>	
						</div>
						<div class="formCont">
							<input name="" type="submit" value="Dale">
						</div>
						<!--input name="" type="submit" value="ENVIAR"-->
					</form>
					<div class="formCont">
						<input name="" id="submitSth" type="submit" value="Create">
						<input name="" id="deleteSth" type="submit" value="Delete">
						<input name="" id="updateSth" type="submit" value="Update">	
					</div>
					<div id="roundButtonsCont">	
						<div class="btn-round back"><span><</span></div>
						<div class="btn-round next"><span>></span></div>
					</div>
				</div>
			</div>
		</section>
		<iframe name="pokeregistro"></iframe>
		<footer></footer>
	</div>
</body>
<script src="../<?php echo _JS; ?>code.js"></script>
</html>



