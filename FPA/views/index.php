<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Pokemon Ligue</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/jquery.js"></script>
	<script src="./js/contenido.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oxygen:700' rel='stylesheet' type='text/css'>
</head>
<body>
	<div id="container">
		<header>
			<div id="firstHeaderCont">
				<div id="imgHeaderCont">
					<!--img src="Charizard.png" alt=""--><h1>
					POKEMON</h1>
				</div>
			</div>
			<nav>
				<div id="navInsideCont">
					<ul>
						<li><a href ="<?php echo _URL; ?>index">INDEX</a></li><li id="registro">
						<a href     ="">REGISTER</a><ul id="alternative"><li>
						<a href     ="<?php echo _URL; ?>Register/crear">LIGUE</a></li><li>
						<a href     ="<?php echo _URL; ?>Others/crear">OTHERS</a></li></ul></li>
						<li><a href     ="">COLISSEUM</a></li>
					</ul>
				</div>
			</nav>
		</header>
		<section id="imgSection">
			<div id="imgSectionCont">
				<img src="./assets/bg.png" alt="Pokemon">
			</div>
		</section>
		<section class="formSection">
			<div id="indexCont" class="contForm">
				<h2>Desarrollo web avanzado</h2>
				<p>Juan Camilo Salazar</p>
				<p>Camilo Andres Hurtado</p>
				<p>Andres Guerra</p>
				<p>Sebastian Vargas</p>
			</div>
		</section>
		<footer></footer>
	</div>
</body>
<script src="./<?php echo _JS; ?>code.js"></script>
</html>



