<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Pokemon Ligue</title>
	<link rel="stylesheet" type="text/css" href="../<?php echo _CSS; ?>style.css">
	<script src="../<?php echo _JS; ?>jquery.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oxygen:700' rel='stylesheet' type='text/css'>
</head>
<body>
	<div id="container">
		<header>
			<div id="firstHeaderCont">
				<div id="imgHeaderCont">
					<!--img src="Charizard.png" alt=""--><h1>
					POKEMON</h1>
				</div>
			</div>
			<nav>
				<div id="navInsideCont">
					<ul>
						<li><a href ="<?php echo _URL; ?>index">INDEX</a></li><li id="registro">
						<a href     ="">REGISTER</a><ul id="alternative"><li>
						<a href     ="crear">LIGUE</a></li><li>
						<a href     ="<?php echo _URL; ?>Others/crear">OTHERS</a></li></ul></li>
						<li><a href     ="">COLISSEUM</a></li>
					</ul>
				</div>
			</nav>
		</header>
		<!--div id="imgBack"></div-->
		<section id="introductionSection">
			<div id="registerTitle">
				<h2>Welcome Trainer</h2>
				<h3>Inscriptions</h3>
			</div>
		</section>

		<section id="formSection">
			<div id="contForm">
				<div id="contHideForm">
					<form id="trainerForm" name="" method="POST" action="" target="" enctype="">
						<div class="formCont">
							<h3>Entrenador</h3>
							<p>Llene el formulario con informacionm real y asegurese de llenar todos los campos</p>
						</div>
						<div class="formCont">
							<input name ="id" placeholder="Id" type="number" required>	
							<input name ="name" placeholder="Nombre" type="text" required>	
							<input name ="second_name" placeholder="Segundo nombre" type="text" required>	
							<input name ="age" placeholder="Edad" type="number" required>		
							<select name ="City_id" required>
								<option value="">Seleccione una ciudad</option>
								<?php 
									foreach ($vars["cities"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["city"]."</option>";
									}
								?>
							</select>
							<select name ="Sex_id" required>
								<option value="">Seleccione un sexo</option>
								<?php 
									foreach ($vars["sexs"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["sex"]."</option>";
									}
								?>
							</select>
						</div>
						<div class="fileCont">
							<label for  ="Foto">PHOTO</label>	
							<input name ="photo" id="Foto" placeholder="Foto" type="file">
						</div>
						<!--input name="" type="submit" value="ENVIAR"-->
					</form>
					<form id="pokemonForm" name="" method="POST" action="" target="" enctype="">

						<div class="formCont">
							<h3>Pokemon</h3>
							<p>Asegurese de llenar el formulario de manera correcta, recuerde hay valores que
								son opcionales en este formulario, el primer pokemon que ingrese sera su principal</p>
						</div>
						<div class="formCont">
							<input name ="name" placeholder="Nombre" type="text" required>	
							<input name ="weight" placeholder="Peso" type="number" required>	
							<select name ="Sex_id" required>
								<option value="">Seleccione un sexo</option>
								<?php 
									foreach ($vars["sexs"] as $key => $value) {
										echo "<option value='".$value["id"]."'>".$value["sex"]."</option>";
									}
								?>
							</select>
							<input name ="lp" placeholder="Puntos de vida" type="number" required>
							<select name ="invo" required>
								<option value="">Seleccione una involución</option>
							</select>
							<select name ="evo" required>
								<option value="">Seleccione una evolución</option>
							</select>
							<select name ="mega" required>
								<option value="">Seleccione una mega-evolución</option>
							</select>
							<select name ="Type_id 1" required>
									<option value="">Seleccione un tipo</option>
									<?php 
										foreach ($vars["types"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
										}
									?>
							</select>
							<select name ="Type_id 2" required>
									<option value="">Seleccione un tipo</option>
									<?php 
										foreach ($vars["types"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["type"]."</option>";
										}
									?>
							</select>
							<select name ="Attack 1" required onchange="attackFinder(this)">
									<option value="">Seleccione primer ataque</option>
									<?php 
										foreach ($vars["attacks"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["name"]."</option>";
										}
									?>
							</select>
							<select name ="Attack 2" required onchange="attackFinder(this)">
									<option value="">Seleccione segundo ataque</option>
									<?php 
										foreach ($vars["attacks"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["name"]."</option>";
										}
									?>
							</select>
							<select name ="Attack 3" required onchange="attackFinder(this)">
									<option value="">Seleccione tercer ataque</option>
									<?php 
										foreach ($vars["attacks"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["name"]."</option>";
										}
									?>
							</select>
							<select name ="Attack 4" required onchange="attackFinder(this)">
									<option value="">Seleccione cuarto ataque</option>
									<?php 
										foreach ($vars["attacks"] as $key => $value) {
											echo "<option value='".$value["id"]."'>".$value["name"]."</option>";
										}
									?>
							</select>
						</div>

						<div class="fileCont">
							<label for  ="Foto">PHOTO</label>	
							<input name ="photo" id="Foto" placeholder="Foto" type="file">
						</div>
	
						<!--input name="" type="submit" value="ENVIAR"-->
					</form>
					<form id="enviarForm" name="" method="POST" action="save" target="" enctype="">
						<div class="formCont">
							<h3>Dale play</h3>
							<p>Nos fuimos.</p>
						</div>						
					</form>
					<div id="roundButtonsCont">	
						<div class="btn-round next"><span>></span></div>
						<div class="btn-round plus"><span>+</span></div>
					</div>
				</div>
			</div>
		</section>
		<iframe name="pokeregistro"></iframe>
		<footer></footer>
		<input name=" 	" data-id="" id="banderaId">
	</div>
</body>
<script src="../<?php echo _JS; ?>code.js"></script>
</html>



