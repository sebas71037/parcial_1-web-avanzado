var contadorPokemon=0;
var dateTrainer = {};
var pokemons = [];
var forms=$('form');
//var trainerElement = {};
//var pokemonsElement = [];
var elements = {};
var banderaId;

$(document).ready(function(){

});

$('#registro').hover(function(){

	$("#alternative").css("display", "block");
});

$('#registro').mouseleave(function(){

	$("#alternative").css("display", "none");
});

$('#roundButtonsCont .btn-round.plus').click(function(){

	var banderaContinuar = checkValues();
	if(banderaContinuar == true){
		getFormPokemon();
		contadorPokemon++;
		if(contadorPokemon <= 10){
			$("form:visible").fadeOut(400, "linear", function(){
				if(contadorPokemon >= 6) $('#roundButtonsCont .btn-round.next').css('background-color','#FFB300');	
			});	
			$("form:visible").fadeIn(400, 'linear', function(){});
		}
	}	 
});



$('#deleteSth').click(function(){

	var actualForm = $("form:visible").attr("id");
	$('select:not([name="id"]), input:not([type="submit"]), textarea').fadeOut(400, "linear", function(){
		$('select[name="id"]').fadeIn(400, 'linear', function(){});
	});

	var name = $('#'+actualForm).attr('name');
	console.log(name);
	$('#'+actualForm).attr('action', 'delete/'+name);
});

$('#submitSth').click(function(){
	var actualForm = $("form:visible").attr("id");
	$('select[name="id"]').fadeOut(400, "linear", function(){
		$('select:not([name="id"]), input, textarea').fadeIn(400, 'linear', function(){});
	});

	var name = $('#'+actualForm).attr('name');
	$('#'+actualForm).attr('action', 'save/'+name);
});

$('#updateSth').click(function(){

	var actualForm = $("form:visible").attr("id");
	$('#' + actualForm +' select, input, textarea').fadeIn();

	var name = $('#'+actualForm).attr('name');
	$('#'+actualForm).attr('action', 'update/'+name);
});

$('#roundButtonsCont .btn-round.next').click(function(){

	var banderaContinuar = checkValues();
	var idVal = idGet();
	var actualForm = $("form:visible").attr("id");

	var banderaEntrar = checkValues();


	if( banderaEntrar==true && actualForm == 'trainerForm' ){
		idAsker(idVal)
		.done(function(r) {

		    if( r == 0 ) {
		    	getFormTrainer();
				moveOut($(".btn-round.next"));  
		    }
		})
		.fail(function(x) {
	    
		});	
	}

	if( actualForm != 'pokemonForm' && actualForm != 'trainerForm'){
		moveOut(this);  
	}else if( actualForm == 'pokemonForm' && contadorPokemon >= 6){

		//Listo para enviar
			elements = {
						pepe: JSON.stringify(dateTrainer),
						pepe2: JSON.stringify(pokemons)
			};

			$.ajax({type: "POST",
                    url:"save",
                    data: elements,
                    cache: false,                
                    success: function(response)
                    {
                        console.log(response);
                    } 
					
					});

		moveOut(this);  
	}
});

function idGet(){

	var actualForm = $("form:visible").attr("id");
	var value;

	$("#"+actualForm+" input").each(function(){

		if($(this).attr('name').toLowerCase() == 'id'){

			value = {id: $(this).val()};
		}
	});	
	return value;
}

function idAsker(value, callback){

	return $.ajax({type: "POST",
        url:"checkId",
        data: value,
        cache: false
        });
}

$('#roundButtonsCont .btn-round.back').click(function(){

	moveOut(this);
});

function moveOut(button){

	var cont=0;
	var actualForm = $("form:visible").attr("id");
	var sth = 1;

	//¿Donde estoy?

	$('form').each(function(){
		if($(this).attr('id') == $("form:visible").attr("id")) return false;
		cont++;
	});

	//Cambiar entre avanzar o volver

	if( $(button).attr('class').split(" ")[1] == 'back' ) sth = -1;
	else if( $(button).attr('class').split(" ")[1] == 'next' ) sth = 1;

	$("#contHideForm").fadeOut(400, "linear", function(){

		//Inicio comportamiento botones

		if(actualForm == 'trainerForm'){ $('#roundButtonsCont .btn-round.plus').css('display', 'block'); 
		$('#roundButtonsCont .btn-round.next').css('background-color','#BDBDBD');
		}else if( actualForm == 'pokemonForm' ) $('#roundButtonsCont').css('display', 'none');
		else $('#roundButtonsCont .btn-round.plus').css('display', 'none');

		//Inicio cambio de form

		if(actualForm == forms[0].id && sth == -1){ $('form').eq(forms.length-1).css('display', 'block'); $("#"+actualForm).css('display', 'none');}
		else if(actualForm == forms[forms.length-1].id && sth == 1){ $('form').eq(0).css('display', 'block'); $("#"+actualForm).css('display', 'none');}
		else{ $('form').eq(cont+sth).css('display', 'block'); $('form').eq(cont).css('display', 'none');}

	});	
	$('#contHideForm').fadeIn(400, 'linear', function(){});	
}

function checkValues(){

	var actualForm = $("form:visible").attr("id");
	var contContinuar = 0;
	var contAtaques = 0;
	var contTipos = 0;

	$('#'+actualForm+' input:not([type="submit"]), #'+actualForm+' select').each(function(){

		if($(this).val()==='') contContinuar++;
		
		if($(this).is("select[name^='Attack']") == true && $(this).val()!=='') contAtaques++;
		if($(this).is("select[name^='Type_id']") == true && $(this).val()!=='') contTipos++;

		if($(this).attr("name")=='invo' || $(this).attr("name")=='evo' || $(this).attr("name")=='mega') contContinuar--;
	});

	if( contAtaques >=1 ) contContinuar-=4-contAtaques;
	if( contTipos >= 1 ) contContinuar-=2-contTipos;	

	var banderaContinuar = (contContinuar == 0 ) ? true:false;
	return banderaContinuar;
}

function getFormTrainer(){
	var actualForm = $("form:visible").attr("id");
	var trainerElement = [];
	$('#'+actualForm+' input, #'+actualForm+' select').each(function(){
		trainerElement.push($(this).val());
	});

    dateTrainer = {    id: trainerElement[0],
                       name: trainerElement[1],
                       second_name: trainerElement[2],
                       age: trainerElement[3],
                       City_id: trainerElement[4],
                       Sex_id: trainerElement[5],
                       photo: trainerElement[6]
    };
}



function getFormPokemon(){
	var actualForm = $("form:visible").attr("id");
	var pokemon = [];
	$('#'+actualForm+' input, #'+actualForm+' select').each(function(){
		pokemon.push($(this).val());
	});
	//pokemons.push(pokemon);
	var pokemonElement = {};
	if( contadorPokemon == 0 )
		pokemonElement = {  name: pokemon[0],
				                weight: pokemon[1],
				                Sex_id: pokemon[2],
				                lp: pokemon[3],
				                invo: pokemon[4],
				                evo: pokemon[5],
				                mega: pokemon[6],
				                photo: pokemon[13],
				                principal: 1,
		                	Type : [
		               			pokemon[7],
		               	 		pokemon[8],
		               		],		               
		                	Attack : [
				               	pokemon[9],
				               	pokemon[10],
				               	pokemon[11],
				               	pokemon[12],
		               		]             
		           		}

	else{
		pokemonElement = {  name: pokemon[0],
				                weight: pokemon[1],
				                Sex_id: pokemon[2],
				                lp: pokemon[3],
				                invo: pokemon[4],
				                evo: pokemon[5],
				                mega: pokemon[6],
				                photo: pokemon[13],
				                principal: 0,
		                	Type : [
		               			pokemon[7],
		               	 		pokemon[8],
		               		],		               
		                	Attack : [
				               	pokemon[9],
				               	pokemon[10],
				               	pokemon[11],
				               	pokemon[12],
		               		]             
		           		}

	}
	//pokemonElement = objectNullifier(pokemonElement);
	pokemons.push(pokemonElement);
}