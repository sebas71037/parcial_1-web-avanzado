/*function init() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    // "icons" is defined in icons.js
    // SVG paths have no flag for being filled or not, but GoJS Geometry paths do.
    // We want to mark each SVG path as filled:
    for (var k in icons) {
      icons[k] = go.Geometry.fillPath(icons[k]);
    }

    // a collection of colors
    var colors = {
      blue:   "#00B5CB",
      orange: "#F47321",
      green:  "#C8DA2B",
      gray:   "#888",
      white:  "#F5F5F5"
    }

    // The first Diagram showcases what the Nodes might look like "in action"
    myDiagram = $(go.Diagram, "myDiagram",
                  {
                    initialContentAlignment: go.Spot.Center,
                    "undoManager.isEnabled": true,
                    layout: $(go.TreeLayout)
                  });

    // A data binding conversion function. Given an icon name, return the icon's Path string.
    function geoFunc(geoname) {
      if (icons[geoname]) return icons[geoname];
      else return icons["heart"]; // default icon
    }

    // Define a simple template consisting of the icon surrounded by a filled circle
    myDiagram.nodeTemplate =
      $(go.Node, "Auto",
        $(go.Shape, "Circle",
            { fill: "lightcoral", strokeWidth: 4, stroke: colors["gray"], width: 60, height: 60 },
            new go.Binding("fill", "color")),
        $(go.Panel, "Vertical",
          { margin: 3 },
          $(go.Shape,
            { fill: colors["white"], strokeWidth: 0 },
            new go.Binding("geometryString", "geo", geoFunc))
        ),
        // Each node has a tooltip that reveals the name of its icon
        { toolTip:
            $(go.Adornment, "Auto",
              $(go.Shape, { fill: "LightYellow", stroke: colors["gray"], strokeWidth: 2 }),
              $(go.TextBlock, { margin: 8, stroke: colors["gray"], font: "bold 16px sans-serif" },
                new go.Binding("text", "geo")))
        }
      );

    // Define a Link template that routes orthogonally, with no arrowhead
    myDiagram.linkTemplate =
      $(go.Link,
        { routing: go.Link.Orthogonal, corner: 5, toShortLength: -2, fromShortLength: -2 },
        $(go.Shape, { strokeWidth: 5, stroke: colors["gray"] })); // the link shape

    // Create the model data that will be represented by Nodes and Links
    myDiagram.model = new go.GraphLinksModel(
    [
      { key: 1, geo: "file"          , color: colors["blue"]   },
      { key: 2, geo: "alarm"         , color: colors["orange"] },
      { key: 3, geo: "lab"           , color: colors["blue"]   },
      { key: 4, geo: "earth"         , color: colors["blue"]   },
      { key: 5, geo: "heart"         , color: colors["green"]  },
      { key: 6, geo: "arrow-up-right", color: colors["blue"]   },
      { key: 7, geo: "html5"         , color: colors["orange"] },
      { key: 8, geo: "twitter"       , color: colors["orange"] }
    ],
    [
      { from: 1, to: 2 },
      { from: 1, to: 3 },
      { from: 3, to: 4 },
      { from: 4, to: 5 },
      { from: 4, to: 6 },
      { from: 3, to: 7 },
      { from: 3, to: 8 }
    ]);

  }*/

function init () {

  var $ = go.GraphObject.make;

  var myDiagram =
    $(go.Diagram, "Torneo",
      {
        initialContentAlignment: go.Spot.Center, // center Diagram contents
        "undoManager.isEnabled": true, // enable Ctrl-Z to undo and Ctrl-Y to redo
        layout: $(go.TreeLayout, // specify a Diagram.layout that arranges trees
                  { angle: 90, layerSpacing: 35 })
      });

  // the template we defined earlier
  myDiagram.nodeTemplate =
    $(go.Node, "Horizontal",
      { background: "#F47321" },
      $(go.Picture, 
        { margin: 5, width: 50, height: 50, background: "#DDDDDD" },
        new go.Binding("source"))
    );

  // define a Link template that routes orthogonally, with no arrowhead
  myDiagram.linkTemplate =
    $(go.Link,
      { routing: go.Link.Orthogonal, corner: 3 },
      $(go.Shape, { strokeWidth: 3, stroke: "#555" })); // the link shape

  var model = $(go.TreeModel);
  model.nodeDataArray =
  [
    /*{ key: "1",              source: "" },
    { key: "2", parent: "1", source: "" },
    { key: "3", parent: "1", source: "" },
    { key: "4", parent: "3", source: "img/4.jpg" },
    { key: "5", parent: "3", source: "img/5.jpg" },
    { key: "6", parent: "2", source: "img/6.jpg" },
    { key: "7", parent: "2", source: "img/1.jpg" }*/

    { key: "1", parent: "", source: "" },
    { key: "2", parent: "1", source: "" },
    { key: "3", parent: "1", source: "" },
    { key: "4", parent: "2", source: "" },
    { key: "5", parent: "2", source: "" },
    { key: "6", parent: "3", source: "" },
    { key: "7", parent: "3", source: "" },
    { key: "8", parent: "4", source: "" },
    { key: "9", parent: "4", source: "" },
    { key: "10", parent: "5", source: "" },
    { key: "11", parent: "5", source: "" },
    { key: "12", parent: "6", source: "" },
    { key: "13", parent: "6", source: "" },
    { key: "14", parent: "7", source: "" },
    { key: "15", parent: "7", source: "" },
    { key: "16", parent: "8", source: "" },
    { key: "17", parent: "8", source: "" },
    { key: "18", parent: "9", source: "" },
    { key: "19", parent: "9", source: "" },
    { key: "20", parent: "10", source: "" },
    { key: "21", parent: "10", source: "" },
    { key: "22", parent: "11", source: "" },
    { key: "23", parent: "11", source: "" },
    { key: "24", parent: "12", source: "" },
    { key: "25", parent: "12", source: "" },
    { key: "26", parent: "13", source: "" },
    { key: "27", parent: "13", source: "" },
    { key: "28", parent: "14", source: "" },
    { key: "29", parent: "14", source: "" },
    { key: "30", parent: "15", source: "" },
    { key: "31", parent: "16", source: "" },
    { key: "32", parent: "16", source: "" },
    { key: "33", parent: "17", source: "" },
    { key: "34", parent: "17", source: "" },
    { key: "35", parent: "18", source: "" },
    { key: "36", parent: "18", source: "" },
    { key: "37", parent: "19", source: "" },
    { key: "38", parent: "19", source: "" },
    { key: "39", parent: "20", source: "" },
    { key: "40", parent: "20", source: "" },
    { key: "41", parent: "21", source: "" },
    { key: "42", parent: "21", source: "" },
    { key: "43", parent: "22", source: "" },
    { key: "44", parent: "22", source: "" },
    { key: "45", parent: "23", source: "" },
    { key: "46", parent: "23", source: "" },
    { key: "47", parent: "24", source: "" },
    { key: "48", parent: "24", source: "" },
    { key: "49", parent: "25", source: "" },
    { key: "50", parent: "25", source: "" },
    { key: "51", parent: "26", source: "" },
    { key: "52", parent: "26", source: "" },
    { key: "53", parent: "27", source: "" },
    { key: "54", parent: "27", source: "" },
    { key: "55", parent: "28", source: "" },
    { key: "56", parent: "28", source: "" },
    { key: "57", parent: "29", source: "" },
    { key: "58", parent: "29", source: "" },
    { key: "59", parent: "30", source: "" },
    { key: "60", parent: "30", source: "" }

    /*
1
2-3
4-5/6-7
8-9/10-11/12-13/14-15
16-17/18-19/20-21/22-23/24-25/26-27/28-29/30
31-32/33-34/35-36/37-38/39-40/41-42/43-44/45-46/47-48/49-50/51-52/53-54/55-56/57-58/59-60
    */
  ];
  myDiagram.model = model;
  }
